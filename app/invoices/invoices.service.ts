import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';

@Injectable()
export class InvoicesService {
  invoicesObservable;

  constructor(private af:AngularFire) { }

    addInvoice(invoice){
    this.invoicesObservable.push(invoice);
  }

  getInvoices(){
    this.invoicesObservable = this.af.database.list('/invoices');
    return this.invoicesObservable;
	}

}