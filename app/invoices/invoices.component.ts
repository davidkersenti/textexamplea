import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {

invoices;
addInvoice(invoice){
    this._invoicesService.addInvoice(invoice);
  }

   constructor(private _invoicesService: InvoicesService) {

    //this.users = this._userService.getUsers();
  }

      ngOnInit() {
   this._invoicesService.getInvoices().subscribe(invoicesData => this.invoices = invoicesData);
   }

}