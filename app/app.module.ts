import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireModule} from 'angularfire2';   /// ex 7 class

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoicesService } from './invoices/invoices.service';

const appRoutes:Routes = [    // ex 6 class
  {path:'users',component:UsersComponent},
  {path:'invoices',component:InvoicesComponent},
  {path:'invoice-form',component:InvoiceFormComponent},
  {path:'posts',component:PostsComponent},
  {path:'',component:InvoiceFormComponent},
  {path:'**',component:PageNotFoundComponent},
]


  // Initialize Firebase  ex 7 class
  export const config = {
    apiKey: "AIzaSyBa9A-t-vtMjy77tCSBIVjd_B95lgMC60Q",
    authDomain: "lastt-a13da.firebaseapp.com",
    databaseURL: "https://lastt-a13da.firebaseio.com",
    projectId: "lastt-a13da",
    storageBucket: "lastt-a13da.appspot.com",
    messagingSenderId: "27852157093"
  };


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    UserComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent,
    InvoicesComponent,
    InvoiceFormComponent,
    InvoiceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,  // ex 6 class
    RouterModule.forRoot(appRoutes),  // ex 6 class
    AngularFireModule.initializeApp(config)   /// ex 7 class
  ],
  providers: [UsersService, PostsService,InvoicesService],
  bootstrap: [AppComponent]
  
  
})
export class AppModule { }
